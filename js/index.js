const player = document.getElementById("player");
const currentTime = document.getElementById("current-time");
const totalTime = document.getElementById("total-time");
const progressMusic = document.getElementById("progress-music");
const playBtn = document.getElementById("play-btn");
const pauseBtn = document.getElementById("pause-btn");

const app = {
  start: function() {
    this.playAudio();
    player.ontimeupdate = () => {
      progressMusic.value = player.currentTime;
      progressMusic.min = 0;
      progressMusic.max = player.duration;
      currentTime.textContent = this.convertTime(player.currentTime);
      totalTime.textContent = this.convertTime(player.duration);
    };
  },
  convertTime: function(time) {
    let mins = Math.floor(time / 60);
    let secs = Math.floor(time % 60);

    if (mins < 10) {
      mins = "0" + String(mins);
    }
    if (secs < 10) {
      secs = "0" + String(secs);
    }

    return mins + ":" + secs;
  },
  playAudio: function() {
    player.play();
    pauseBtn.style.display = 'block';
    playBtn.style.display = 'none';
  },
  pauseAudio: function() {
    player.pause();
    pauseBtn.style.display = 'none';
    playBtn.style.display = 'block';
  },
  refreshAudio: function() {
    player.currentTime = 0;
  },
  progressOnClick: function(valueProgress) {
    player.currentTime = valueProgress;
    progressMusic.value = valueProgress;
    this.playAudio();
  }
};

app.start();
